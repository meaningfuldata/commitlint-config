/*
   Copyright 2021 Meaningful Data

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

/**
 * @see README.md
 */
module.exports = {
  rules: {
    /*
     * Rules from `@commitlint/config-conventional`
     */
    // Warning if the body does not begin with a blank line
    "body-leading-blank": [1, "always"],
    // Error if the length of any line of the body surpasses 100 characters.
    "body-max-line-length": [2, "always", 100],
    // Warning if the footer does not begin with a blank line
    "footer-leading-blank": [1, "always"],
    // Error if the length of any line of the footer surpasses 100 characters.
    "footer-max-line-length": [2, "always", 100],
    // Error if the length of the header surpasses 100 characters.
    "header-max-length": [2, "always", 100],
    // Error if the subject is empty
    "subject-empty": [2, "never"],
    // Error if the subject ends with "."
    "subject-full-stop": [2, "never", "."],
    // Error if the type is not lower case.
    "type-case": [2, "always", "lower-case"],
    // Error if the type is not included.
    "type-empty": [2, "never"],

    /*
     * Meaningful Data rules
     */
    // Error if the subject does not follow "Sentence case" (first character is upper case).
    "subject-case": [2, "always", "sentence-case"],
    // Error if the type is not one of this list.
    "type-enum": [
      2,
      "always",
      [
        // Build system, Docker, changes to `package.sjon`, except versions
        "build",
        // Continuous Integration
        "ci",
        // Features
        "feat",
        // Fix a bug
        "fix",
        // Dependencies
        "deps",
        // Documentation
        "docs",
        // Internationalization and translation
        "i18n",
        // Small improvements of existing features
        "improvement",
        // Code style
        "style",
        // Code improvements that do not affect behaviour in any manner
        "refactor",
        // Each version release
        "release",
        // Tests (Includes changes to configuration files)
        "test",
        // Development tools (linters, formatters, etc.)
        "tools",
      ],
    ],
  },
  parserPreset: {
    parserOpts: {
      // Header example: "feat scope: Subject"
      headerPattern: /^(\w+) ?(\w*): (.+)$/,
      headerCorrespondence: ["type", "scope", "subject"],
    },
  },
};
