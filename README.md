# @meaningfuldata/commitling-config

Shareable [`commitlint`](https://github.com/conventional-changelog/commitlint) configuration used by [Meaningful Data](http://meaningfuldata.eu).

This variation of [Conventional Commits](https://www.conventionalcommits.org/) does not wrap scopes with parentheses
and it capitalizes the subject of the commit. So, instead of using

```
feat(my_scope): do something
```

this shorter and easier to read style is used:

```
feat my_scope: Do something
```

By default, no scopes are provided, they have to be explicitly chosen by project:

```js
{
  extends: ['@meaningfuldata/commitlint-config'],
  rules: {
    'scope-enum': [2, 'always', ['scope_a', 'scope_b', 'scope_c']]
  }
}
```

## Install

You can install it with Yarn or NPM:

```sh
# Yarn
yarn add -D @meaningfuldata/commitlint-config

# NPM
npm i -D @meaningfuldata/commitlint-config
```

## License

Apache 2.0
